University of Dayton

Department of Computer Science

CPS 491 - Spring 2022

Dr. Phu Phung

## Capstone II Project Report 

# UD Advancement Legacy Reporting Environment

# Team members

1.  Joe Durham, durhamj4@udayton.edu
2.  Ryan Downes, downesr1@udayton.edu
3.  Max Onkst, onkstm1@udayton.edu
4.  Zihan Liu, liuz45@udayton.edu


# Company Mentors

Doug Bishop, _Director of Information Systems and Technology_

University of Dayton Advancement Services

300 College Park, Dayton, OH

## Project homepage

[cps491s22-team4.bitbucket.io]


# Overview

Raiser’s Edge has been used to track all UD Advancement data since 2005

We will be transitioning to Affinaquest to track to the data

After the transition, we will map the data from the Affinaquest database to the corresponding field in the data warehouse

![Overview Architecture](https://trello.com/1/cards/61bbcde3ab708a6c49ef847f/attachments/61bbcdecdbc61c3f5198cd3e/download/Arch.PNG)

Figure 1. - A Sample of Overview Architecture of the proposed project.

# Project Context and Scope

Summary:

- UD Advancement will be moving to a new CRM in December of 2022. To ensure that the transferring of data goes smoothly, we will be updating the data warehouse to account for the shift in CRM platform. 

Purpose/Impacts:

- UD advancement’s customer information and data will be successfully moved onto a new CRM, and the data warehouse will be updated to take information from there
- Preserve data so UD Advancement can smoothly transition into adding information into the new reporting environment. Instead of starting from scratch

Why did we choose this project?

- University of Dayton focused project
- Company located close-by allowing for close and easy interaction
- Involves database, something we only scratched the surface of in CAPSTONE I
- Features some high-level concepts we are unfamiliar with
- We will be able to see our project’s impact before it is even completely deployed


# High-level Requirements

Programs we will be using:

- MS SQL
    - Relational Database Management System, developed by Microsoft
    - Uses T-SQL language
- Salesforce Advancement Software
    - CRM:  Custom Relationship manager, manages relationships and interactions between company and customers.
    - Raiser’s Edge
    - Data-warehouse: A large centralized storage of company data
        - Structured Data
        - Unstructured Data
- Oracle Database
    - Database management software provided by the company Oracle
    - Runs on Salesforce, a type of CRM designed for University use.
- Transitional Data between two different systems
    - Map, extract, transform, load

# Impacts

- The biggest impact we are most excited about is the impact on the UD community. We are excited to be working on a project that will be used by students, past and present, for a long period of time

- UD Advancement Department will be able to access and edit the relevant data that is collected

- UD Advancement Department will be able to have smooth transition of data

- UD Advancement Department will have ability to concentrate on their main portion of their work, rather than worry about transitioning the data


# Project Management

We will follow the Scrum approach, thus your team needs to identify the task in each sprint cycles, team meeting schedules, including this Fall and next Spring semester. The tenative schedule and sprint cycles for Spring 2022 are as follows. 

Sprint Plans:

- Sprint 0
    - Meet with UD Advancement
    - Gather information on where their Phase I ended
- Sprint 1
    - Receive proper access to appropriate systems
    - Identify tables and fields that need to be built
    - Identify best way to map out data movement
- Sprint 2
    - Get familiar with systems that have been granted accessible
    - Get familiar with data and data fields
- Sprint 3
    - Begin the transition of data
    - Extract data from warehouse
- Sprint 4
    - Store data in new warehouse

![https://trello.com/1/cards/61bbcde3ab708a6c49ef847f/attachments/61bbcf7896059c5606d3aa73/download/Trello.PNG](https://trello.com/1/cards/61bbcde3ab708a6c49ef847f/attachments/61bbcf7896059c5606d3aa73/download/Trello.PNG)  

Trello link: https://trello.com/b/7gc5u1en/capstone-ii-board

Bitbucket link: https://bitbucket.org/cps491s22-team4/cps491s22-team4.bitbucket.io/src/master/


# Company Support

- What we have received:
    - We have received project background information from Douglas Bishop. We have also received detailed progress on how much has been completed by UD Advancement 
- What we will receive:
    - We will be receiving support from members of the IT center.
    - We will be planning on once a week meetings with members of the IT department, though depending on the current issue, it may be more